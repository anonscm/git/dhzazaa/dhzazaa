#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 15:57:54 2020

@author: zabeth
"""
import csv

# Le fichier à lire
fichier="/home/elisabeth/Dev/Virus/donnees-hospitalieres-covid19-2020-04-05-19h00.csv"

nbmorts = []
nbrea = []
nbrad = []
nbhosp = []
jours = []

totaldc = 0


#Retourne le nombre de morts pour un département et un sexe donné
#sous forme de tableau
def depdc(dpt,sex,fichier):
    tab=[]
    avant = 0
    with open(fichier, newline='') as dhcovid:
        lecteur=csv.reader(dhcovid)
        for ligne in lecteur:
            dc=ligne[6]
            if  (ligne[1] == sex) and (ligne[0] == dpt):
                nb = int(dc)
                diff = nb - avant
                avant = nb
                tab.append(nb)
    return tab
                
def jours(dpt,sex,fichier):
    tab=[]
    with open(fichier, newline='') as dhcovid:
        lecteur=csv.reader(dhcovid)
        for ligne in lecteur:
            jour=ligne[2]
            if  (ligne[1] == sex) and (ligne[0] == dpt):
                tab.append(jour)
    return tab
                

def dephosp(dpt,sex,fichier):
    tab=[]
    with open(fichier, newline='') as dhcovid:
        lecteur=csv.reader(dhcovid)
        for ligne in lecteur:
            hosp=ligne[3]
            if  (ligne[1] == sex) and (ligne[0] == dpt):
                tab.append(int(hosp))
    return tab

def deprea(dpt,sex,fichier):
    tab=[]
    with open(fichier, newline='') as dhcovid:
        lecteur=csv.reader(dhcovid)
        for ligne in lecteur:
            hosp=ligne[4]
            if  (ligne[1] == sex) and (ligne[0] == dpt):
                tab.append(int(hosp))
    return tab



def deprad(dpt,sex,fichier):
    tab=[]
    with open(fichier, newline='') as dhcovid:
        lecteur=csv.reader(dhcovid)
        for ligne in lecteur:
            rad=ligne[5]
            if  (ligne[1] == sex) and (ligne[0] == dpt):
                tab.append(int(rad))
    return tab
