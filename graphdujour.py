#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 17:02:33 2020

@author: zabeth
"""

import lecteurcsv,random
import matplotlib.pyplot  as  plt

source="donnees-hospitalieres-covid19-2020-04-08-19h00.csv"
# Le département     
lieu = "71"

# Le sexe
qui = "0"

# Le numéro du jour
num = 21


def barres(quand,quoi,couleur):
    plt.figure(figsize=(10, 10))
    plt.xticks(rotation=90)
    plt.bar(quand, quoi,color=couleur)
    plt.show()

def croix(combien):
    plt.axis("off")
    x=0
    y=0
    for i in range(combien):        
        x=x+5
        if x>100:
            y=y+1
            x=5
        plt.plot(x,y,'b+')
    
def points(combien):
    plt.axis("off")
    for i in range(combien):        
        x=random.randint(0,100)
        y=random.randint(0,100)
        plt.scatter(x,y)
        
def deuxfig(combien,extrait):
    plt.axis("off")
    for i in range(combien):        
        x=random.randint(0,500)
        y=random.randint(0,500)
        if (i<extrait):
            plt.plot(x,y,'rs')
        else:
            plt.plot(x,y,'bs')
 

deces =  lecteurcsv.depdc(lieu,qui,source)
print (deces)
dates = lecteurcsv.jours(lieu,qui,source)
hosp =  lecteurcsv.dephosp(lieu,qui,source)
print (hosp)
rea = lecteurcsv.deprea(lieu,qui,source) 
print (rea)
rad = lecteurcsv.deprad(lieu,qui,source)
print (rad)

plt.figure(figsize=(18, 9))


#barres(dates,deces,(0.2,0.2,0.6,1))
plt.gcf().subplots_adjust(left = 0.1, bottom = 0.1,
                       right = 0.9, top = 0.9, wspace = 0, hspace = 0.1)

plt.suptitle ("Dep "+lieu+" : "+dates[num]) 

plt.subplot(121)
plt.title(str (hosp[num])+" hosp (dont "+str(rea[num])+" rea)")
deuxfig(hosp[num],rea[num])

plt.subplot(222)
plt.title(str (deces[num])+" décès")
croix(deces[num])

plt.subplot(224)
plt.title(str (rad[num])+" retours")
points(rad[num])




